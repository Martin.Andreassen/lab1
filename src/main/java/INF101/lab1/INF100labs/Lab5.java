package INF101.lab1.INF100labs;

import java.util.ArrayList;

/**
 * Implement the methods removeThrees, uniqueValues and addList.
 * These programming tasks was part of lab5 in INF100 fall 2022/2023. You can find them here: https://inf100h22.stromme.me/lab/5/
 */
public class Lab5 {
    
    public static void main(String[] args) {
        // Call the methods here to test them on different inputs

    }

    public static ArrayList<Integer> multipliedWithTwo(ArrayList<Integer> list) {
        ArrayList<Integer> ny_liste = new ArrayList<Integer>();
    
        for (int tall : list) {
            ny_liste.add(tall * 2);
        }
    
        System.out.println(ny_liste);
    
        return ny_liste;
    }
    


        
    

    public static ArrayList<Integer> removeThrees(ArrayList<Integer> list) {

        ArrayList<Integer> ny_liste = new ArrayList<Integer>();

        for (int tall : list) {
            
            if (tall != 3) {
            
                ny_liste.add(tall);

            }
        }

        System.out.println(ny_liste);
    
        return ny_liste;
        
    }





        
    

    public static ArrayList<Integer> uniqueValues(ArrayList<Integer> list) {

        ArrayList<Integer> ny_liste = new ArrayList<Integer>();

        for (int tall : list) {
            
            if (!ny_liste.contains(tall)) {
                
                ny_liste.add(tall);

            }
        }

        System.out.println(ny_liste);
    
        return ny_liste;

        
    }

    public static void addList(ArrayList<Integer> a, ArrayList<Integer> b) {

        if (a.size() != b.size()) {
            throw new IllegalArgumentException("Listene må være av samme lengde.");
        }
    
     
        for (int i = 0; i < a.size(); i++) {
            a.set(i, a.get(i) + b.get(i));
        }
    
        
    }

}