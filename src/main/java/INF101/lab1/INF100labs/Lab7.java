package INF101.lab1.INF100labs;

import java.util.ArrayList;

/**
 * Implement the methods removeRow and allRowsAndColsAreEqualSum.
 * These programming tasks was part of lab7 in INF100 fall 2022/2023. You can find
 * them here: https://inf100h22.stromme.me/lab/7/
 */
public class Lab7 {

    public static void main(String[] args) {
        // Call the methods here to test them on different inputs

    }

    public static void removeRow(ArrayList<ArrayList<Integer>> grid, int row) {
        if (row >= 0 && row < grid.size()) {
            grid.remove(row);
        }

        
        for (ArrayList<Integer> rad : grid) {
            System.out.println(rad);
        }
    }




        
    

    public static boolean allRowsAndColsAreEqualSum(ArrayList<ArrayList<Integer>> grid) {

        int n = grid.size();
        if (n == 0) return true;

        int[] radSum = new int[n];
        int[] kolonneSum = new int[n];

       
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < grid.get(i).size(); j++) {
                radSum[i] += grid.get(i).get(j);
                kolonneSum[j] += grid.get(i).get(j);
            }
        }

       
        int førsteSum = radSum[0];
        for (int sum : radSum) {
            if (sum != førsteSum) return false;
        }
        for (int sum : kolonneSum) {
            if (sum != førsteSum) return false;
        }

        return true;
    }



       
    }

